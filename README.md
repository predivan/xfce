# README #

[Xfce](http://xfce.org) repository for [Crux Linux](https://crux.nu)

# Current status #

To paraphrase Doug from 'Forged in Fire', "It will build",
and I've been dogfooding it for a while now.
Caveat emptor still applies, though.

As of 2017-03-05, repo is signed.
(public key is RWRDjyquJXO46viA0LVkc6vXIuFg93DI/7kSdqgwQqRt3EB7tAET8eUt)

xfce.pub is signed with my GPG key, for no reason other than 'crypto is cool'.

# NOTE #

This repo is fork of sepen et al. Xfce repo, which is,
as of 2017-01-29, unmaintained, but who did a great initial work,
for which I am thankful.

Applications, plugins etc. that had no activity in the Xfce git repo
in the last 18 months apart from translation updates, and/or are
unmaintained upstream, were sent to /dev/null.
If/when their development continues, they might be re-imported.

All maintainer lines in Pkgfiles are modified, since all the bugs
are mine.

# BRANCHES #

3.4-xfce-next branch is the main one,
~~expect footprint mismatches with 3.2~~ (3.2 branch was nuked)
don't bother with master and vanilla 3.4.
3.4 branch is reasonably up to date xfce-4.12, but since I can't test it
anymore, it's pretty much an archive now, all work goes into 3.4-xfce-next,
which contains 4.13 development (4.14-to-be one day).

# TODO #

* ~~xfce4-mixer is dead upstream, replace it with...something (PA plugin,
  perhaps?)~~
* clean up and test
